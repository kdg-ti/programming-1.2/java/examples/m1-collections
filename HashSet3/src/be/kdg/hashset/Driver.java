package be.kdg.hashset;

public class Driver {
    private String name;
    private int number;
    
    public Driver(String name, int number) {
        this.name = name;
        this.number = number;
    }
    
    @Override
    public boolean equals(Object o) {
        return number == ((Driver) o).number;
    }
    
    @Override
    public String toString() {
        return String.format("%-10s %2d", name, number);
    }
}

