package be.kdg.hashset;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo3 {
    public static void main(String[] args) {
        Set<Driver> drivers = new HashSet<>();
        drivers.add(new Driver("Kevin", 20));
        drivers.add(new Driver("Carlos", 55));
        drivers.add(new Driver("Nico", 6));
        drivers.add(new Driver("Valtteri", 6));
        System.out.printf("%d drivers: %s", drivers.size(), drivers);
    }
}

/*
[Kevin      20, Nico        6, Valtteri    6, Carlos     55]
 */