package be.kdg.hashmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapWordCounter {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a sentence: ");
        String sentence = scanner.nextLine();
        String[] words = sentence.split(" ");
        
        for (String string : words) {
            String word = string.toLowerCase();
            if (map.containsKey(word)) {
                int amount = map.get(word);
                map.put(word, amount + 1);
            } else {
                map.put(word, 1);
            }
        }
        
        System.out.printf("\nMap contains:\n%-10s %10s\n", "Key", "Value");
        for (String key : map.keySet()) {
            System.out.printf("%-10s %10d\n", key, map.get(key));
        }
        System.out.printf("\nsize: %d\nisEmpty: %b\n", map.size(), map.isEmpty());
    }
}
