package be.kdg.list;

import java.util.ArrayList;
import java.util.List;

public class ArrayList2 {
    public static void main(String[] args) {
        List<String> myStringList = new ArrayList<>();
        myStringList.add(0, "Alfa");
        myStringList.add(0, "Bravo");
        myStringList.add(0, "Charlie");
        myStringList.add(0, "Delta");
        
        for (int i = 0; i < myStringList.size(); i++) {
            System.out.print(myStringList.get(i) + " ");
        }
        
        System.out.println();
        System.out.println(myStringList.size());
    }  // main()
}

/*
Delta Charlie Bravo Alfa 4
 */

