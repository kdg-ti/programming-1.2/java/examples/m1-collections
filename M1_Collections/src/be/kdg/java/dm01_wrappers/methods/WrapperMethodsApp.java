package be.kdg.java.dm01_wrappers.methods;

public class WrapperMethodsApp {
    
    public static void main(String[] args) {
        /*
         * String --> primitive value
         */
        final String STR_VALUE = "127";
        int value = Integer.valueOf(STR_VALUE);
        System.out.printf("Integer.valueOf(\"%s\") = %d \n", STR_VALUE, value);
        value = Integer.parseInt(STR_VALUE);
        System.out.printf("Integer.parseInt(\"%s\") = %d \n", STR_VALUE, value);
        
        double dbl = Double.parseDouble("123.45");
        System.out.printf("Double.parseDouble(\"123.45\") = %.2f \n", dbl);
        System.out.println();
        
        /*
         * toHex/toBinary
         */
        final int INT_VALUE = 127;
        String strValue = Integer.toHexString(INT_VALUE);
        System.out.printf("Integer.toHexString(%d) --> %s \n", INT_VALUE, strValue);
        strValue = Integer.toBinaryString(INT_VALUE);
        System.out.printf("Integer.toBinaryString(%d) --> %s \n", INT_VALUE, strValue);
        System.out.println();
    }  // main()
}
