package be.kdg.sorting;

public class Driver implements Comparable<Driver> {
    private String name;
    private int number;

    public Driver(String name, int number) {
        setName(name);
        setNumber(number);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int compareTo(Driver other) {
        return this.name.compareTo(other.name);
    }

    @Override
    public String toString() {
        return String.format("%-10s %2d", name, number);
    }
}

