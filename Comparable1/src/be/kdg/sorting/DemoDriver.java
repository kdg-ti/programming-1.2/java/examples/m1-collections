package be.kdg.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoDriver {
    public static void main(String[] args) {
        List<Driver> drivers = new ArrayList<>();
        drivers.add(new Driver("Kevin", 20));
        drivers.add(new Driver("Carlos", 55));
        drivers.add(new Driver("Nico", 6));
        drivers.add(new Driver("Lewis", 44));
        drivers.add(new Driver("Daniel", 3));
        drivers.add(new Driver("Stoffel", 2));
        drivers.add(new Driver("Max", 33));
        drivers.add(new Driver("Sebastian", 5));
        drivers.add(new Driver("Sergio", 11));
        drivers.add(new Driver("Fernando", 14));
        drivers.add(new Driver("Romain", 8));
        // ...
        Collections.sort(drivers);
        // ...
        System.out.println(drivers);
    }
}
