package be.kdg.list;

import java.util.ArrayList;
import java.util.List;

public class ListCopy4 {
    public static void main(String[] args) {
        List<String> myList = List.of("Alfa", "Bravo", "Charlie", "Delta");
        
        // UnsupportedOperationException
        // myList.set(2, "Beta");
        List<String> fullAccessList = new ArrayList<>(myList);
        fullAccessList.set(1, "Beta");
        
        System.out.println(fullAccessList);
    }
}
