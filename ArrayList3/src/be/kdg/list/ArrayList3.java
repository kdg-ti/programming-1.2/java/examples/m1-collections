package be.kdg.list;

import java.util.*;

public class ArrayList3 {
    public static void main(String[] args) {
        List<String> myStringList = new ArrayList<>();
        myStringList.add("Alfa");
        myStringList.add("Bravo");
        myStringList.add("Charlie");
        myStringList.add("Delta");
        
        for (Iterator<String> it = myStringList.iterator(); it.hasNext(); ) {
            System.out.print(it.next() + " ");
        }
        
        System.out.println();
        System.out.println(myStringList.size());
    }  // main()
}



