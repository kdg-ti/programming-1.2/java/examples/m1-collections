package be.kdg.sorting;

public class Movie implements Comparable<Movie> {
    private String title;
    private int year;

    public Movie(String title, int year) {
        setTitle(title);
        setYear(year);
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public int getYear() {
        return year;
    }
    
    public void setYear(int year) {
        this.year = year;
    }
    
    @Override
    public int compareTo(Movie other) {
        return year - other.year;
        // return Integer.compare(year, other.year);
    }

    @Override
    public String toString() {
        return String.format("%-32s %4d", title, year);
    }
}
