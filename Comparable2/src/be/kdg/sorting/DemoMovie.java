package be.kdg.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoMovie {
    public static void main(String[] args) {
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie("The Godfather", 1972));
        movies.add(new Movie("The Shawshank Redemption", 1994));
        movies.add(new Movie("Schindler's List", 1993));
        movies.add(new Movie("Raging Bull", 1980));
        movies.add(new Movie("Casablanca", 1942));
        movies.add(new Movie("Citizen Kane", 1941));
        movies.add(new Movie("Gone with the Wind", 1939));
        movies.add(new Movie("The Wizard of Oz", 1939));
        movies.add(new Movie("One Flew Over the Cuckoo's Nest", 1975));
        movies.add(new Movie("Lawrence of Arabia", 1962));
        movies.add(new Movie("Vertigo", 1958));
        movies.add(new Movie("Psycho", 1960));
        // ...
        Collections.sort(movies);
        // ...
        movies.forEach(System.out::println);
    }
}
