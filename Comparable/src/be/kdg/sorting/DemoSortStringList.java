package be.kdg.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DemoSortStringList {
    public static void main(String[] args) {
        List<String> aList = new ArrayList<>(List.of("The", "quick", "brown", "fox", "jumps", "over", "the", "lazy",
                "dog"));
        
        System.out.println("unsorted: " + aList);
        Collections.sort(aList);
        System.out.println("sorted:   " + aList);
    }
}

/*
[The, brown, dog, fox, jumps, lazy, over, quick, the]
 */