package be.kdg;

import java.util.Iterator;

/**
 * Author: Jan de Rijke
 */
public class boxing {

	private static void callMe(long nr){
		System.out.println("long wins!");
	}

	private static void callMe(Integer nr){
		System.out.println("Wrapper wins!");
	}

	private static void callMeToo(Long nr){
		System.out.println("Long wrapper wins!");
	}

	public static void main(String[] args) {
		int nr= 5;
		callMe(nr);
		// 	Long wins!



		// The following line does not work. Why is that?
		// callMeToo(6);
	}
}
