package be.kdg.hashset;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class HashSetDemo1 {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        Collections.addAll(set, "red", "red", "white", "blue", "green", "gray", "white", "orange", "green");
        
        System.out.printf("%d colours: %s\n", set.size(), set);
    }
}
