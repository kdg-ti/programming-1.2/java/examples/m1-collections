import java.util.*;

public class SpeedTest {
    private Random random = new Random();
    
    private List<Integer> anArrayList = new ArrayList<>();
    private List<Integer> aLinkedList = new LinkedList<>();
    
    private final int TEST_AMOUNT; // How many numbers should be sorted
    private final int MAX_NUMBER;  // Wat is the highest possible number in the list
    
    public SpeedTest(int amount) {
        TEST_AMOUNT = amount;
        MAX_NUMBER = 1000;
    }
    
    private void createLists() {
        anArrayList.clear();
        aLinkedList.clear();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            int randomNumber = random.nextInt(MAX_NUMBER);
            anArrayList.add(randomNumber);
            aLinkedList.add(randomNumber);
        }
    }
    
    public SpeedTestResult testAddLast() {
        anArrayList.clear();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            anArrayList.add(random.nextInt(MAX_NUMBER));
        }
        long arrayListTime = System.currentTimeMillis() - startTime;
        
        aLinkedList.clear();
        startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            aLinkedList.add(random.nextInt(MAX_NUMBER));
        }
        long linkedListTime = System.currentTimeMillis() - startTime;
        
        return new SpeedTestResult("add last", this.TEST_AMOUNT, arrayListTime, linkedListTime);
    }
    
    public SpeedTestResult testAddFirst() {
        anArrayList.clear();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            anArrayList.add(0, random.nextInt(MAX_NUMBER));
        }
        long arrayListTime = System.currentTimeMillis() - startTime;
        
        aLinkedList.clear();
        startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            aLinkedList.add(0, random.nextInt(MAX_NUMBER));
        }
        long linkedListTime = System.currentTimeMillis() - startTime;
        
        return new SpeedTestResult("add first", this.TEST_AMOUNT, arrayListTime, linkedListTime);
    }
    
    public SpeedTestResult testRemoveFirst() {
        createLists();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            anArrayList.remove(0);
        }
        long arrayListTime = System.currentTimeMillis() - startTime;
        
        startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            aLinkedList.remove(0);
        }
        long linkedListTime = System.currentTimeMillis() - startTime;
        
        return new SpeedTestResult("remove first", this.TEST_AMOUNT, arrayListTime, linkedListTime);
    }
    
    public SpeedTestResult testGet() {
        createLists();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            anArrayList.get(random.nextInt(MAX_NUMBER));
        }
        long arrayListTime = System.currentTimeMillis() - startTime;
        
        startTime = System.currentTimeMillis();
        for (int i = 0; i < TEST_AMOUNT; i++) {
            aLinkedList.get(random.nextInt(MAX_NUMBER));
        }
        long linkedListTime = System.currentTimeMillis() - startTime;
        
        return new SpeedTestResult("get", this.TEST_AMOUNT, arrayListTime, linkedListTime);
    }
    
    public SpeedTestResult testSort() {
        createLists();
        long startTime = System.currentTimeMillis();
        Collections.sort(anArrayList);
        long arrayListTime = System.currentTimeMillis() - startTime;
        
        startTime = System.currentTimeMillis();
        Collections.sort(aLinkedList);
        long linkedListTime = System.currentTimeMillis() - startTime;
        
        return new SpeedTestResult("sort", this.TEST_AMOUNT, arrayListTime, linkedListTime);
    }
}
