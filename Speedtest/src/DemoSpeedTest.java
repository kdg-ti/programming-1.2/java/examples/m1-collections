public class DemoSpeedTest {
    
    public static void main(String[] args) {
        SpeedTest speedTest = new SpeedTest(100000);
    
        SpeedTestResult resultTestAddLast = speedTest.testAddLast();
        System.out.println(resultTestAddLast.toString());
    
        SpeedTestResult resultTestAddFirst = speedTest.testAddFirst();
        System.out.println(resultTestAddFirst);
        
        System.out.println(speedTest.testRemoveFirst());
        System.out.println(speedTest.testGet());
        System.out.println(speedTest.testSort());
    }  // main()
    
}
