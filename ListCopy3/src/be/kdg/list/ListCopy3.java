package be.kdg.list;

import java.util.List;

public class ListCopy3 {
    public static void main(String[] args) {
        List<String> myList = List.of("Alfa", "Bravo", "Charlie", "Delta");
        String[] myArray = myList.toArray(new String[0]);
        
        for (String word : myArray) {
            System.out.print(word + " ");
        }
        System.out.println();
    }  // main()
}
