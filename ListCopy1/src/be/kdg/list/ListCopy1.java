package be.kdg.list;

import java.util.ArrayList;
import java.util.List;

public class ListCopy1 {
    public static void main(String[] args) {
        String[] myArray = { "Alfa", "Bravo", "Charlie", "Delta" };
        List<String> myList = new ArrayList<>();
        
        for (String name : myArray) {
            myList.add(name);
        }
        System.out.println("List => Array: " + myList);
        
        System.out.print("Array => List:" );
        String [] copyArray = new String [myList.size()];
        for (int i=0;i<myList.size();i++){
            copyArray[i] = myList.get(i);
            System.out.print(" " + copyArray[i] );
        }
    }  // main()
}

/*
    List => Array: [Alfa, Bravo, Charlie, Delta]
    Array => List: Alfa Bravo Charlie Delta
*/