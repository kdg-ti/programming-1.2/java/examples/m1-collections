package be.kdg.linked;

import java.util.*;

public class LinkedList {
    public static void main(String[] args) {

        List<String> colourList = new java.util.LinkedList<>(List.of( "green", "blue", "purple", "red"));
        List<String> grayList = new java.util.LinkedList<>(List.of("black","white","gray","silver"));

        colourList.addAll(grayList);

        System.out.println("The colours: " + colourList);
        colourList.subList(4, 6).clear();
        ((java.util.LinkedList<String>)colourList).addFirst("yellow");
        System.out.println("Changed colours: " + colourList);

        ListIterator<String> iterator = colourList.listIterator(colourList.size());
        System.out.print("Colours in reverse: ");
        while (iterator.hasPrevious()) {
            System.out.print(iterator.previous() + " ");
        }
    }
}