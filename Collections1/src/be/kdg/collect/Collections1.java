package be.kdg.collect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Collections1 {
    public static void main(String[] args) {
        List<String> fruitList = new ArrayList<>(List.of("banana", "pear", "apple", "prune", "coconut"));
        Collections.sort(fruitList);
        System.out.println("sorted: " + fruitList);
        Collections.shuffle(fruitList);
        System.out.println("shuffled: " + fruitList);
        Collections.reverse(fruitList);
        System.out.println("reversed: " + fruitList);
        System.out.println("min: " + Collections.min(fruitList));
        System.out.println("max: " + Collections.max(fruitList));
        Collections.sort(fruitList);
        System.out.println("banana at: " + Collections.binarySearch(fruitList, "banana"));
        System.out.println("cucumber at: " + Collections.binarySearch(fruitList, "cucumber"));
    }
}
